<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php  

    //task1
    $string = "Welcome to PHP";

      $reversed = "";
      $tmp = "";
      for($i = 0; $i < strlen($string); $i++) {
          if($string[$i] == " ") {
              $reversed .= $tmp . " ";
              $tmp = "";
              continue;
          }
          $tmp = $string[$i] . $tmp;    
      }
      $reversed .= $tmp;
      
      echo $reversed;
      echo '<pre>';
    //task2

     $arr = [2, 3, 4, 6, 7, 9, 11, 20];

     function even($array){
       for ($i=0; $i<count($array); $i++){
         if($array[$i]%2!=0){
           array_splice($array, $i,1);
           $i--;
          }
       }
       foreach($array as $value){
          echo $value;
          echo ' ';
       }
      }

      $filter_arr = fn ($arr) => even($arr);

      $filter_arr($arr);
      echo '<pre>';
    //task3
    function sum(...$num) {
      $total = 0;
      foreach ($num as $temps) {
          $total += $temps;
      }
      return $total;
  }
      echo sum (2,3,4);
      echo "<pre>";

    ?>
</body>
</html>